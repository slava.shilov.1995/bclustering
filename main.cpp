#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <thread>
#include <vector>
#include <mutex>
#include <algorithm>
#include <conio.h>
#include <future>
#include <time.h>

using namespace std;

#define SW << setw(2) << 
#define SOL vector<pair<vector<int>, vector<int>>> 
string files[] = { "20x20", "24x40", "30x50", "30x90", "37x53", "5x7" };

string filename = files[5];//0-5
int numThreads = 5;
bool showMatrix = true;
bool printError = false;
bool autoSave = true;
bool independentThreads = false;

pair<bool, double> CheckSol(SOL& sol);
void ShowMatrix(vector<vector<bool>> mtrx);
vector<vector<bool>> matrix;

struct SOLUTION {
	SOL sol;
	bool valid;
	double cost;

	void Validate() {
		auto p = CheckSol(sol);
		valid = p.first;
		cost = p.second;
	}

	void Show() {//i dont give a fckk how this func actually works. just some magic and lots of my shitcode.
		vector<int> mv(matrix.size(), -1);
		vector<int> dv(matrix[0].size(), -1);
		auto mat = matrix;
		int it = 0;
		for (int p = 0; p < sol.size(); ++p) {
			for (int m = 0; m < sol[p].first.size(); ++m) {
				for (int i = 0; i < mat[0].size(); ++i) {
					mat[it][i] = matrix[sol[p].first[m]][i];
					mv[it] = p;
				}
				it++;
			}
		}
		it = 0;
		auto matm = mat;
		for (int p = 0; p < sol.size(); ++p) {
			for (int d = 0; d < sol[p].second.size(); ++d) {
				for (int i = 0; i < mat.size(); ++i) {
					mat[i][it] = matm[i][sol[p].second[d]];
					dv[it] = p;
				}
				it++;
			}
		}
		int mm = mat.size();
		int md = mat[0].size();
		cout << sol.size() << " clusters:" << endl;
		for (int i = 0; i < mm; ++i) {
			for (int j = 0; j < md; ++j) {
				if (mv[i] == dv[j]) {
					cout SW(mat[i][j] ? "|#" : "|-");
				} else {
					cout SW(mat[i][j] ? "#" : "-");
				}
				
			}
			cout << endl;
		}
	}
};

int mm, md;//matrix machines and matrix details
double numOnes = 0;
SOLUTION best_sol;
mutex best_sol_mutex;
bool shutdown = false;

void ReadData() {
	ifstream file(filename + ".txt");
	if (file.is_open()) {
		int m, d;
		string line;
		getline(file, line);
		istringstream iss(line);
		iss >> m >> d;
		mm = m;
		md = d;
		matrix = vector<vector<bool>>(m, vector<bool>(d, false));
		int im = 0;
		while (getline(file, line)) {
			iss = istringstream(line);
			iss >> d;
			while (iss >> d) {
				matrix[im][d - 1] = true;
				++numOnes;
			}
			++im;
		}
		file.close();
	} else {
		cout << "Error: cant open " << filename << ".txt" << endl;
	}
}

void ShowMatrix(vector<vector<bool>> mtrx) {
	for (int i = 0; i < mm; ++i) {
		for (int j = 0; j < md; ++j) {
			cout SW(mtrx[i][j] ? "#" : "-");
		}
		cout << endl;
	}
}

bool IsVectorTrue(vector<bool>& vec) {
	for (int i = 0; i < vec.size(); ++i) {
		if (!vec[i]) return false;
	}
	return true;
}

void SafeSave(string fn);

bool TryGladden(SOLUTION sol) {
	if (sol.valid) {
		best_sol_mutex.lock();
		if (sol.cost > best_sol.cost) {
			best_sol = sol;
			cout << "New best: " << best_sol.cost << endl;
			if (autoSave) SafeSave(filename);
			best_sol_mutex.unlock();
			return true;
		}
		best_sol_mutex.unlock();
	}
	return false;
}

SOLUTION GetStartSolution(int numStartClusters) {
	srand(time(NULL));
	vector<bool> unsMachines = vector<bool>(mm, false);
	vector<bool> unsDetails = vector<bool>(md, false);

	SOL sol;

	while (!IsVectorTrue(unsMachines)) {
		int mIndex;
		for (int i = mm - 1; i >= 0; --i) {
			if (!unsMachines[i]) {
				mIndex = i;
				break;
			}
		}
		if (IsVectorTrue(unsDetails)) {
			sol[sol.size() - 1].first.push_back(mIndex);
			unsMachines[mIndex] = true;
		} else {
			int numUnsMac = 0;
			for (int i = 0; i < mm; ++i) {
				if (!unsMachines[i]) {
					++numUnsMac;
				}
			}
			int cSize = min(numUnsMac, mm / numStartClusters);

			sol.push_back(pair<vector<int>, vector<int>>(vector<int>(1, mIndex), vector<int>()));
			bool ok = false;
			for (int i = 0; i < md; ++i) {
				if (matrix[mIndex][i] && !unsDetails[i]) {
					sol[sol.size() - 1].second.push_back(i);
					unsDetails[i] = true;
					ok = true;
				}
			}
			if (!ok) {
				sol.erase(sol.end() - 1);
				int bestCluster;
				int bestClusterCost = -mm * md;
				for (int c = 0; c < sol.size(); ++c) {
					int cost = 0;
					for (int m = 0; m < sol[c].first.size(); ++m) {
						for (int i = 0; i < md; ++i) {
							if (matrix[mIndex][i] == matrix[sol[c].first[m]][i]) {
								++cost;
							} else {
								--cost;
							}
						}
					}
					if (cost > bestClusterCost) {
						bestClusterCost = cost;
						bestCluster = c;
					}
				}
				sol[bestCluster].first.push_back(mIndex);
				unsMachines[mIndex] = true;
				continue;
			}
			unsMachines[mIndex] = true;
			--cSize;

			while (cSize > 0) {
				int bestCandidate;
				int bestCandidateCost = -mm * md;
				for (int k = 0; k < mm; ++k) {
					if (!unsMachines[k]) {
						int cost = 0;
						for (int i = 0; i < md; ++i) {
							for (int j = 0; j < sol[sol.size() - 1].first.size(); ++j) {
								if (matrix[k][i] == matrix[sol[sol.size() - 1].first[j]][i]) {
									++cost;
								} else {
									--cost;
								}
							}
						}
						if (cost > bestCandidateCost) {
							bestCandidateCost = cost;
							bestCandidate = k;
						}
					}
				}
				sol[sol.size() - 1].first.push_back(bestCandidate);
				unsMachines[bestCandidate] = true;
				//for (int i = 0; i < md; ++i) {
				//	if (matrix[bestCandidate][i] && !unsDetails[i]) {
				//		sol[sol.size() - 1].second.push_back(i);
				//		unsDetails[i] = true;
				//	}
				//}
				--cSize;
			}
		}
	}

	if (!IsVectorTrue(unsDetails)) {
		for (int i = 0; i < md; ++i) {
			if (!unsDetails[i]) {
				sol[sol.size() - 1].second.push_back(i);
				unsDetails[i] = true;
			}
		}
	}

	auto p = CheckSol(sol);

	return { sol, p.first, p.second };
}

void Output(SOLUTION s, string fn) {
	ofstream file(fn + ".sol");
	if (file.is_open()) {
		vector<int> m(mm, -1);
		vector<int> d(md, -1);
		for (int p = 0; p < s.sol.size(); ++p) {
			for (int i = 0; i < s.sol[p].first.size(); m[s.sol[p].first[i++]] = p);
			for (int i = 0; i < s.sol[p].second.size(); d[s.sol[p].second[i++]] = p);
		}
		for (int i = 0; i < mm; ++i) {
			file << m[i] << " ";
		}
		file << endl;
		for (int i = 0; i < md; ++i) {
			file << d[i] << " ";
		}
		file.close();
		cout << "Best solution saved to " << fn << ".sol" << endl;

	} else {
		cout << "Error: cant write file " << fn + ".sol" << endl;
	}
}

pair<bool, double> CheckSol(SOL& sol) {
	vector<bool> unsMachines(mm, false);
	vector<bool> unsDetails(md, false);

	double oneIn = 0;
	double nullIn = 0;

	for (int p = 0; p < sol.size(); ++p) {
		if (sol[p].first.empty()) {
			if (printError) {
				cout << "Cluster " << p << " has no machines!" << endl;
			}
			return { false, -1 };
		}
		if (sol[p].second.empty()) {
			if (printError) {
				cout << "Cluster " << p << " has no details!" << endl;
			}
			return { false, -1 };
		}

		for (int m = 0; m < sol[p].first.size(); ++m) {
			for (int d = 0; d < sol[p].second.size(); ++d) {
				if (matrix[sol[p].first[m]][sol[p].second[d]]) {
					++oneIn;
				} else {
					++nullIn;
				}
			}
			if (unsMachines[sol[p].first[m]]) {
				if (printError) {
					cout << "One machine in two clusters!" << endl;
				}
				return { false, -1 };
			} else {
				unsMachines[sol[p].first[m]] = true;
			}
		}
		for (int d = 0; d < sol[p].second.size(); ++d) {
			if (unsDetails[sol[p].second[d]]) {
				if (printError) {
					cout << "One detail in two clusters!" << endl;
				}
				return { false, -1 };
			} else {
				unsDetails[sol[p].second[d]] = true;
			}
		}
	}

	if (!IsVectorTrue(unsMachines)) {
		if (printError) {
			cout << "Not all machines clustered!" << endl;
		}
		return { false, -1 };
	}
	if (!IsVectorTrue(unsDetails)) {
		if (printError) {
			cout << "Not all details clustered!" << endl;
		}
		return { false, -1 };
	}

	return { true, (oneIn / (numOnes + nullIn)) };
}

bool TryImprove(SOLUTION& s, SOLUTION& candidate, SOLUTION& current, bool& improve) {
	//bad but true for vns
	s.Validate();
	if (s.cost > candidate.cost) {
		candidate = s;
		improve = true;
		return true;
	}
	return false;

	//good and fast but not that we need //this way was used in calculations
	/*s.Validate();
	if (s.cost > current.cost) {
		current = s;
		candidate = current;
		improve = true;
		return true;
	}
	return false;*/
}

SOLUTION LocalSearch(SOLUTION sol) {
	SOLUTION candidate = sol;
	const bool printSomething = false;
	bool improved = true;
	while (improved) {
		improved = false;
		sol = candidate;
		//mswap
		for (int p1 = 0; p1 < sol.sol.size() - 1; ++p1) {
			for (int p2 = p1 + 1; p2 < sol.sol.size(); ++p2) {
				for (int m1 = 0; m1 < sol.sol[p1].first.size(); ++m1) {
					for (int m2 = 0; m2 < sol.sol[p2].first.size(); ++m2) {
						auto s = sol;
						int c = s.sol[p1].first[m1];
						s.sol[p1].first[m1] = s.sol[p2].first[m2];
						s.sol[p2].first[m2] = c;

						TryImprove(s, candidate, sol, improved);
					}
				}
			}
		}
		//dswap
		for (int p1 = 0; p1 < sol.sol.size() - 1; ++p1) {
			for (int p2 = p1 + 1; p2 < sol.sol.size(); ++p2) {
				for (int d1 = 0; d1 < sol.sol[p1].second.size(); ++d1) {
					for (int d2 = 0; d2 < sol.sol[p2].second.size(); ++d2) {
						auto s = sol;
						int c = s.sol[p1].second[d1];
						s.sol[p1].second[d1] = s.sol[p2].second[d2];
						s.sol[p2].second[d2] = c;
						
						TryImprove(s, candidate, sol, improved);
					}
				}
			}
		}
		//minsert
		for (int p1 = 0; p1 < sol.sol.size(); ++p1) {
			for (int p2 = 0; p2 < sol.sol.size(); ++p2) {
				for (int m1 = 0; m1 < sol.sol[p1].first.size(); ++m1) {
					if (p1 != p2) {
						auto s = sol;
						s.sol[p2].first.push_back(s.sol[p1].first[m1]);
						s.sol[p1].first.erase(s.sol[p1].first.cbegin() + m1);
						
						TryImprove(s, candidate, sol, improved);
					}
				}
			}
		}
		//dinsert
		for (int p1 = 0; p1 < sol.sol.size(); ++p1) {
			for (int p2 = 0; p2 < sol.sol.size(); ++p2) {
				for (int d1 = 0; d1 < sol.sol[p1].second.size(); ++d1) {
					if (p1 != p2) {
						auto s = sol;
						s.sol[p2].second.push_back(s.sol[p1].second[d1]);
						s.sol[p1].second.erase(s.sol[p1].second.cbegin() + d1);
						
						TryImprove(s, candidate, sol, improved);
					}
				}
			}
		}
		//singledrop
		for (int p = 0; p < sol.sol.size(); ++p) {
			for (int m = 0; m < sol.sol[p].first.size(); ++m) {
				for (int d = 0; d < sol.sol[p].second.size(); ++d) {
					auto s = sol;
					s.sol.emplace_back(vector<int>(1, s.sol[p].first[m]), vector<int>(1, s.sol[p].second[d]));
					s.sol[p].first.erase(s.sol[p].first.begin() + m);
					s.sol[p].second.erase(s.sol[p].second.begin() + d);

					bool b = TryImprove(s, candidate, sol, improved);
					if (b) {
						m = 0;
						d = 0;
					}
				}
			}
		}
		//split
		for (int p = 0; p < sol.sol.size(); ++p) {
			for (int m = 0; m < sol.sol[p].first.size(); ++m) {
				for (int d = 0; d < sol.sol[p].second.size(); ++d) {
					auto s = sol;
					vector<int> v1m, v1d, v2m, v2d;
					for (int i = 0; i < m; v1m.push_back(s.sol[p].first[i++]));
					for (int i = 0; i < d; v1d.push_back(s.sol[p].second[i++]));
					for (int i = m; i < s.sol[p].first.size(); v2m.push_back(s.sol[p].first[i++]));
					for (int i = d; i < s.sol[p].second.size(); v2d.push_back(s.sol[p].second[i++]));
					s.sol[p].first = v1m;
					s.sol[p].second = v1d;
					s.sol.emplace_back(v2m, v2d);

					TryImprove(s, candidate, sol, improved);
				}
			}
		}
		//merge
		for (int p1 = 0; p1 < sol.sol.size() - 1; ++p1) {
			for (int p2 = p1 + 1; p2 < sol.sol.size(); ++p2) {
				auto s = sol;
				for (int i = s.sol[p2].first.size() - 1; i >= 0; --i) {
					s.sol[p1].first.push_back(s.sol[p2].first[i]);
				}
				for (int i = s.sol[p2].second.size() - 1; i >= 0; --i) {
					s.sol[p1].second.push_back(s.sol[p2].second[i]);
				}
				s.sol.erase(s.sol.begin() + p2);

				TryImprove(s, candidate, sol, improved);
			}
		}
	}
	
	return sol;
}

SOLUTION Shake(SOLUTION sol) {
	int w = sol.sol.size() / 4, h = numThreads;
	int splits = w - h / 2 + rand() % h;
	int merges = w - h / 2 + rand() % h;
	for (int k = 0; k < splits; ++k) {
		int p = rand() % sol.sol.size();
		if (sol.sol[p].first.size() >= 2 && sol.sol[p].second.size() >= 2) {
			int m = 1 + (rand() % (sol.sol[p].first.size() - 1));
			int d = 1 + (rand() % (sol.sol[p].second.size() - 1));
			vector<int> v1m, v1d, v2m, v2d;
			for (int i = 0; i < m; v1m.push_back(sol.sol[p].first[i++]));
			for (int i = 0; i < d; v1d.push_back(sol.sol[p].second[i++]));
			for (int i = m; i < sol.sol[p].first.size(); v2m.push_back(sol.sol[p].first[i++]));
			for (int i = d; i < sol.sol[p].second.size(); v2d.push_back(sol.sol[p].second[i++]));
			sol.sol[p].first = v1m;
			sol.sol[p].second = v1d;
			sol.sol.emplace_back(v2m, v2d);
		}
	}
	for (int k = 0; k < merges; ++k) {
		if (sol.sol.size() >= 2) {
			int p1 = rand() % sol.sol.size();
			int p2;
			do {
				p2 = rand() % sol.sol.size();
			} while (p1 == p2);
			for (int i = sol.sol[p2].first.size() - 1; i >= 0; --i) {
				sol.sol[p1].first.push_back(sol.sol[p2].first[i]);
			}
			for (int i = sol.sol[p2].second.size() - 1; i >= 0; --i) {
				sol.sol[p1].second.push_back(sol.sol[p2].second[i]);
			}
			sol.sol.erase(sol.sol.begin() + p2);
		}
	}
	
	sol.Validate();
	if (!sol.valid) {
		cout << "lol it sucks" << endl;
	}
	return sol;
}

SOLUTION Branch(SOLUTION sol) {
	return LocalSearch(Shake(sol));
}

double CheckCurrentOutput(string fn, bool show) {
	ifstream file(fn + ".sol");
	if (file.is_open()) {
		vector<int> vm;
		vector<int> vd;
		string line;
		getline(file, line);
		istringstream iss(line);
		int value;
		int mvalue = -1;
		while (iss >> value) {
			vm.push_back(value);
			mvalue = max(value, mvalue);
		}
		getline(file, line);
		iss = istringstream(line);
		while (iss >> value) {
			vd.push_back(value);
		}
		SOLUTION s;
		s.sol = vector<pair<vector<int>, vector<int>>>(mvalue + 1);
		for (int i = 0; i < vm.size(); ++i) s.sol[vm[i]].first.push_back(i);
		for (int i = 0; i < vd.size(); ++i) s.sol[vd[i]].second.push_back(i);
		s.Validate();
		file.close();

		if (show) {
			s.Show();
		}

		return s.cost;
	} else {
		return 0;
	}
	return 0;
}

void SafeSave(string fn) {
	double r = CheckCurrentOutput(fn, false);
	cout << best_sol.cost << " => " << r;
	if (best_sol.cost > r) {
		cout << " Allowed." << endl;
		Output(best_sol, fn);
	} else {
		cout << " Prohibited." << endl;
		cout << "Task failed successfully!" << endl;
	}
	cout << endl;
}

void KeyController() {
	double r;
	while (!shutdown) {
		int c = _getch();
		switch (c) {
		case 27://esc
			shutdown = true;
			break;
		case 'o'://o - output
			Output(best_sol, filename);
			break;
		case 'v'://visualization
			best_sol.Show();
			break;
		case 'c'://check output
			cout << "Checking " << filename << ".sol file" << endl;
			r = CheckCurrentOutput(filename, false);
			if (r > 0) {
				cout << "Cost: " << r << endl;
			} else {
				cout << "Error: file does not exist i guess";
			}
			break;
		case 's'://overwrite .sol only if its better than existing .sol file
			cout << "Safety output" << endl;
			SafeSave(filename);
			break;
		case 'x'://check and show
			cout << "Checking " << filename << ".sol file" << endl;
			r = CheckCurrentOutput(filename, true);
			if (r > 0) {
				cout << "Cost: " << r << endl;
			} else {
				cout << "Error: file does not exist i guess";
			}
		}
		
		//cout << c;
	}
}

void IndependentThread() {
	srand(time(NULL));
	best_sol_mutex.lock();
	SOLUTION sol = best_sol;
	best_sol_mutex.unlock();
	while (!shutdown) {
		sol = Branch(sol);
		
	}
}

int main() {
	srand(time(NULL));
	ReadData();
	if (showMatrix) ShowMatrix(matrix);
	SOLUTION start_sol;
	start_sol = GetStartSolution(1);
	for (int i = 2; i < md; ++i) {
		SOLUTION sol = GetStartSolution(i);
		if (sol.valid) {
			if (sol.cost > start_sol.cost) {
				start_sol = sol;
			}
		}
	}
	cout << start_sol.cost << endl;
	best_sol = start_sol;
	start_sol = LocalSearch(start_sol);
	TryGladden(start_sol);
	
	thread controller(KeyController);

	if (independentThreads) {
		vector<thread> trds;
		for (int i = 0; i < numThreads; trds.push_back(thread(IndependentThread)), ++i);
		for (int i = 0; i < numThreads; trds[i++].join());
	} else {
		SOLUTION localBest = best_sol;
		while (!shutdown) {
			vector<future<SOLUTION>> trds(numThreads);
			for (int i = 0; i < numThreads; ++i) {
				trds[i] = async(&Branch, localBest);
			}
			localBest = start_sol;
			for (int i = 0; i < numThreads; ++i) {
				SOLUTION s = trds[i].get();
				TryGladden(s);
				if (s.cost > localBest.cost) {
					localBest = s;
				}
			}
		}
	}

	controller.join();

	return 0;
}
